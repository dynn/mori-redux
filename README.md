# mori-redux

## Install instructions
1. `cd` into your project directory
2. run `npm install --save mori-redux`

## Usage
### createReduce
```javascript
// import the goodies we like to use from mori
import { hashMap, list, map, inc, dec } from 'mori'
// import the goodies we like to use from mori-redux
import { createReducer } from 'mori-redux'

// setup some state to begin with
const initialState = list(1, 2, 3)

// along with the initialState we provide a hashMap with actionHandlers keyed by action-types
const reducer = createReducer(initialState, hashMap(
  'increase', (state, action) => map(inc, state),
  'decrease', (state, action) => map(dec, state)
))

// for simplicity I wont hassle with setting up a store
// lets give our reducer an action which is a plain javascript object 
const changedState = reducer(initialState, {type: 'increase'})

changedState.toString() // (2, 3, 4)

// we can also use a hashMap as action like this
const changedAgain = reducer(changedState, hashMap('type', 'decrease')

changedAgain.toString() // (1, 2, 3)
```

### combineReducers
```javascript
// import mori stuff
import { hashMap, inc, dec } from 'mori'
// import the combineReducer function
// and for convience the createReducer function
import { combineReducers, createReducer } from 'mori-redux'

// let's quickly setup 2 reducers

const lightSwitch = createReducer(false, hashMap(
  'turn_off', (state, action) => false,
  'turn_on', (state, action) => true
))

const counter = createReducer(0, hashMap(
  'increase', (state, action) => inc(state),
  'decrease', (state, action) => dec(state)
))

// Now we combine the reducers
const reducer = combineReducers(hashMap(
  'light', lightSwitch,
  'counter', counter
))

// Lets make use of them
const changedState = reducer(undefined, hashMap('type', 'turn_on'))

changedState.toString() // {'light' true, 'counter' 0}

const changedAgain = reducer(changedState, hashMap('type', 'increase'))

changedAgain.toString() // {'light' true, 'counter' 1}

```

### logMiddleware
```javascript
// Import our middleware
import { logMiddleware  } from 'mori-redux'
// Import the Redux stuff we need
import { createStore, applyMiddleware } from 'redux'

// Lets mockup a reducer
const reducer = () => 'state'

// Apply the middleware when creating the store
const store = createStore(reducer, applyMiddleware(logMiddeware))

// It's done, whenever the state change, it will log the state (in a human readable way)
```

## Todos
There will be more goodies in the future, keep track of you favorite functionality by subscribing to the related issue.

## Contributors
- Frank van Vuuren <frank@dynngroup.com>

## Uses
* [Foto op aluminium](https://www.aluminiumland.nl)
* [Foto op glas](https://www.fotoopglasland.nl)
* [Tuinposter](https://www.tuinposterland.nl)
* [foto op canvas](https://www.canvasland.nl)
