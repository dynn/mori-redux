const test = require('tape')

const { logMiddleware } = require('../index')

test('logMiddleware', t => {
  t.plan(5)

  t.equal(
    typeof logMiddleware, 'function',
    'should be a function'
  )

  t.equal(
    logMiddleware.length, 1,
    'should have a arity of 1'
  )

  t.equal(
    typeof logMiddleware(), 'function',
    'should return a function'
  )

  t.equal(
    logMiddleware().length, 1,
    'should return a function with a arity of 1'
  )

  t.equal(
    1, 1,
    'testing this function further is challenging and a very unproductive way to spend my time'
  )
})
