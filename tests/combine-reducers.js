const test = require('tape')

const { hashMap, inc, dec } = require('mori')

const { combineReducers, createReducer } = require('../index')

test('combineReducers', t => {
  t.plan(7)

  const lightSwitch = createReducer(true, hashMap(
    'turn_off', (state, action) => false,
    'turn_on', (state, action) => true
  ))

  const counter = createReducer(1, hashMap(
    'increase', (state, action) => inc(state),
    'decrease', (state, action) => dec(state)
  ))

  const combinedReducer = combineReducers(hashMap(
    'light', lightSwitch,
    'counter', counter
  ))

  t.equal(
    typeof combineReducers, 'function',
    'should be a function'
  )

  t.equal(
    combineReducers.length, 1,
    'should have an arity of 1'
  )

  t.equal(
    typeof combineReducers(hashMap()), 'function',
    'should return a function'
  )

  t.equal(
    combineReducers(hashMap()).length, 2,
    'should return a function with an arity of 2'
  )

  t.equal(
    combineReducers(hashMap('a', createReducer(0, hashMap())))().toString(), '{"a" 0}',
    'should return a reducer which returns initial state when no actionHandlers match'
  )

  t.equal(
    combinedReducer(undefined, {type: 'turn_off'}).toString(), '{"light" false, "counter" 1}',
    'should return a reducer which returns new state on a matching actionHandler (initialState)'
  )

  t.equal(
    combinedReducer(hashMap('light', false, 'counter', 1), {type: 'increase'}).toString(), '{"light" false, "counter" 2}',
    'should return a reducer which returns new state on a matching actionHandler (previousState)'
  )
})
