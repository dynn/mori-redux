const { toClj, isMap, hasKey, get, hashMap, assoc, reduceKV } = require('mori')

exports.createReducer = (initialState, actionHandlers) => {
  const actionHandlersMap = isMap(actionHandlers)
    ? actionHandlers
    : toClj(actionHandlers)
  return (previousState, action) => {
    const state = previousState !== undefined
      ? previousState
      : initialState
    if (action === undefined) return state
    return hasKey(actionHandlersMap, action.type)
      ? get(actionHandlersMap, action.type)(state, action)
      : state
  }
}

exports.combineReducers = (reducersMap) => {
  const initialState = reduceKV(
    (acc, key, val) => assoc(acc, key, val()),
    hashMap(),
    reducersMap
  )
  return (previousState, action) => {
    const state = previousState !== undefined
      ? previousState
      : initialState
    return reduceKV(
      (acc, key, val) => assoc(acc, key, val(get(state, key), action)),
      hashMap(),
      reducersMap
    )
  }
}

exports.logMiddleware = store => next => action => {
  console.group(action.type)
  console.log('dispatching', action)
  const result = next(action)
  console.log('next state', store.getState().toString())
  console.groupEnd()
  return result
}
